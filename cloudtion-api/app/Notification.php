<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static firstOrCreate(array $input)
 * @method static where(string $string, $id)
 */
class Notification extends Model
{
    //
    protected $table = 'notifications';

    protected $fillable = [
        'user_id', 'notification_title', 'notification_body', 'read'
    ];

    protected $hidden = ['created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
