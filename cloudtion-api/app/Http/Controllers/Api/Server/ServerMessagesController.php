<?php

namespace App\Http\Controllers\Api\Server;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreMessageRequest;
use App\Notification;
use App\Server;
use Illuminate\Http\JsonResponse;

class ServerMessagesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index()
    {
        return Notification::where('user_id', auth()->user()->id)->get();
    }

    /**
     * @param StoreMessageRequest $request
     * @return JsonResponse
     */
    public function store(StoreMessageRequest $request)
    {
        $validated = $request->validated();

        if (!$validated) {
            return response()->json(['error' => 'Er is een fout opgetreden tijdens de validatie.'], 403);
        }

        $input = [
            'user_id' => auth()->id(),
            'description' => $request->description
        ];

        $message = Notification::firstOrCreate($input);


        return response()->json($message, 201);

    }
}
