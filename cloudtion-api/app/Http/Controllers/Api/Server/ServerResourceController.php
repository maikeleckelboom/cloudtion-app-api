<?php

namespace App\Http\Controllers\Api\Server;

use App\Http\Requests\UpdateIndexRequest;
use App\Http\Requests\UpdateStatusRequest;
use App\User;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateServerRequest;
use App\Server;
use Exception;


class ServerResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index()
    {
        return Server::where('user_id', auth()->user()->id)->get();
    }

    /**
     * Show the specified resource in view.
     *
     * @param Server $server
     * @return Server
     */
    public function show(Server $server)
    {
        return $server;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param CreateServerRequest $request
     * @param $id
     * @return JsonResponse
     */
    public function store(CreateServerRequest $request)
    {
        $validated = $request->validated();

        if (!$validated) {
            return response()->json(['error' => 'Er is een fout opgetreden tijdens de validatie.'], 403);
        }



        $input = [
            'initialized' => false,
            'order' => null,
            'user_id' => auth()->id(),
            'type' => $request->type,
            'username' => $request->username,
            'password' => $request->password,
            'status' => $request->status
        ];

        $server = Server::firstOrCreate($input);

        activity()
            ->performedOn($server)
            ->log('Server ' . $server->type . ' is aangevraagd.');


        return response()->json($server, 201);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param UpdateStatusRequest $request
     * @param Server $server
     * @return JsonResponse
     */
    public function update(UpdateStatusRequest $request, Server $server)
    {
        $input = [
            'status' => $request->status
        ];

        $server->update($input);

        activity()
            ->performedOn($server)
            ->log('Server ' . $server->type . ' is geüpdatet.');


        return response()->json($server, 200);
    }


    /**
     * Reset the specified server.
     *
     * @param Server $server
     * @return JsonResponse
     */
    public function reset(Server $server)
    {

        activity()
            ->performedOn($server)
            ->log('Server ' . $server->type . ' is gereset.');


        return response()->json(["message" => "$server->type is gereset."], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Server $server
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Server $server)
    {
        if ($server->user_id !== auth()->user()->id) {
            return response()->json('Unauthorized', 401);
        }

        $server->delete();

        /**
         * Log Activity
         */
        activity()
            ->performedOn($server)
            ->log('Server ' . $server->type . ' is verwijderd.');


        return response()->json('Server ' . $server->type . ' is verwijderd.', 200);
    }

//    /**
//     * Destroy completed
//     *
//     * @param Request $request
//     * @return JsonResponse
//     */
//    public function destroyCompleted(Request $request)
//    {
//
//        $serversToDelete = $request->servers;
//
//        $userServerIds = auth()->user()->servers->map(function ($server) {
//            return $server->id;
//        });
//
//        $valid = collect($serversToDelete)->every(function ($value, $key) use ($userServerIds) {
//            return $userServerIds->contains($value);
//        });
//
//        if (!$valid) {
//            return response()->json('Unauthorized', 401);
//        }
//
//        $request->validate([
//            'servers' => 'required|array',
//        ]);
//
//        Server::destroy($request->servers);
//
//        return response()->json('Deleted', 200);
//    }
}
