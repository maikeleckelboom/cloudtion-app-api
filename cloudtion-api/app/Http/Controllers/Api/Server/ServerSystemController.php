<?php

namespace App\Http\Controllers\Api\Server;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateIndexRequest;
use App\Http\Requests\UpdateReadyRequest;
use App\Http\Requests\UpdateStatusRequest;
use App\Server;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ServerSystemController extends Controller
{

    public function index() {
        return Server::where('user_id', auth()->user()->id)->get();
    }


    /**
     * Show the specified resource in view.
     *
     * @param Server $server
     * @return Server
     */
    public function show(Server $server)
    {
        return $server;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateIndexRequest $request
     * @param Server $server
     * @return JsonResponse
     */
    public function update(UpdateIndexRequest $request, Server $server)
    {

        $validated = $request->validated();

        if (!$validated) {
            return response()->json(['error' => 'Er is een fout opgetreden tijdens de validatie.'], 403);
        }

//        $server = Server::find(auth()->user()->id);

        $input = [
            'order' => $request->order,
        ];

        if ($server->update($input)) {
            return response()->json(['message' => 'updated' . $server->type . 'index naar: ' . $request->index]);
        };

        return response()->json(['server' => $server], 403);

    }
}
