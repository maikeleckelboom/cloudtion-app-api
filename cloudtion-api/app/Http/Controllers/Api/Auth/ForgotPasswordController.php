<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * @param Request $request
     * @param $response
     * @return Application|ResponseFactory|Response
     */
    protected function sendResetLinkResponse(Request $request, $response)
    {

//        activity()
//            ->performedOn()
//            ->log('Je hebt een reset-password token ontvangen in je mailbox.');

        return response(['message' => trans($response)], 200);
    }

    /**
     * @param Request $request
     * @param $response
     * @return Application|ResponseFactory|Response
     */
    protected function sendResetLinkFailedResponse(Request $request, $response)
    {

        return response(['errors' => trans($response)], 422);

    }
}
