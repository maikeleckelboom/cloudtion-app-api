<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;


/**
 * @group Auth endpoints
 */
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * @var object
     */
    private $client;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->client = DB::table('oauth_clients')->where('id', 2)->first();
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstname' => ['required', 'string', 'max:255'],
            'lastname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

    }

    /**
     * The user has been registered.
     *
     * @param Request $request
     * @return mixed
     */
    protected function registered(Request $request)
    {
        // get the user
        $user = $request->user();

        // get access_token and refresh_token
        $response = $this->getTokens($request);

        // if getting tokens failed
        if (!$response) {
            return false;
        }

        // decode Json to array
        $json_object = (array)json_decode($response->getContent());

        // set new property user
        $json_object['user'] = $user;

        // encode array to json
        $response->setContent(json_encode($json_object));


        // log Activity
        activity()
            ->performedOn($user)
            ->log('Je account is geregistreerd.');

        // return response
        return $response;

    }

    /**
     * Handle a registration request for the application.
     *
     * @bodyParam name string required The name of the user. Example: Demo
     * @bodyParam email email required The email of the user. Example: demo@demo.com
     * @bodyParam password password required The password of the user. Example: password
     * @bodyParam password_confirmation password required The password confirmation of the user. Example: password
     *
     * @response 200 {
     *    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiNDc5OTg4MjFkMTZlZjUxOTRjOWZhYWFkMzFlZDY0NzljMzliZWMzMjg1NjU2YWViNzdkMmM3ZGMwOGNhNGFiOTZiOTVkZmEwNTE3OWE0ZTciLCJpYXQiOjE1OTAzOTEzNDMsIm5iZiI6MTU5MDM5MTM0MywiZXhwIjoxNjIxOTI3MzQzLCJzdWIiOiIyIiwic2NvcGVzIjpbXX0.onsWJtrF9UT2XEbSsYQbVLvr-TZKGbqIoj4w5W-sEqbrcGep-mRuHJDY1-tY7E_-KxSV3yTrOAtyWFIv51atVFs5m6abF-QWNUpGYMlfEhjQbN6S5QOLXD7deiatSGH0dIXX6v7j7IdUeLgJ5t_6z7oD2s0bAuzfhrHCM9wf7Plyqv7p4-E6ROJ5Atv6IzFFA8dA6eEZWqF_SwOXJMEqyo3Gas7AzNoUSVear8d2sToLZFUv9lPXubp3_5kNN65Ri7bVQXJh0GqCBNN2ySWlO1ODaiIoNPSMOYpBLUaERRTh2AVzDLMvEcKQ5HQFLSqA1wFzABlOweF-7O1mvzdYLSmx8yCvlrIZxnBE2-c69IGmSJKowoczc2lNp96hNept6K-h94xQomC_bd8RajojBP928x-NLPhCH2bg98He0np_xBkm6M91z6M1ZnReZ7s45ViPOTovR6nW0QuqmH6LdF6JEeLc026DKLDX7Ap7fGjq-jFH-tbB_jp0wGGoJfTBLQllftTz9f86oqTXCf85_4fnMgTxB2qX_LBfJw4s6oa1Oex-EpBEprM4C0Awtlo9IkNNRBKLhxa7wPwMHFR_y9w7ZgEbq2pd-qDb4WMcDFfR5mTpCcYYrhufHSa0gnDDDAOanVSaFf58V5T_kKnb72_md7JFf87rhOoSjML_1Ks",
     *    "user": {
     *        "id": 2,
     *        "name": "Demo",
     *        "email": "demo@demo.com",
     *        "email_verified_at": null,
     *        "created_at": "2020-05-25T06:21:47.000000Z",
     *        "updated_at": "2020-05-25T06:21:47.000000Z"
     *    }
     * }
     * @response status=422 scenario="Validation error" {
     *    "message": "The given data was invalid.",
     *    "errors": {
     *        "email": [
     *            "The email field is required."
     *        ]
     *    }
     * }
     *
     * @param Request $request
     * @return Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        if ($response = $this->registered($request)) {
            return $response;
        }

        return $request->wantsJson()
            ? new Response('', 201)
            : redirect($this->redirectPath());
    }
    /**
     * Get the Laravel Passport Auth Tokens
     *
     * @response 200 {
     * "token_type": "Bearer",
     * "expires_in": 86398,
     * "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiMmYyZmJiMDliYjdmM2ZmMTg2MTYzMzdlMjgzNmMxYjBjZTUxZjgyOTY1YzMzNDE1MTc5M2RmMmE0YWNiZTc0NDA3YzAyMmZjZjJhODU0NzUiLCJpYXQiOjE1OTU4ODY4MTEsIm5iZiI6MTU5NTg4NjgxMSwiZXhwIjoxNTk1OTczMjA5LCJzdWIiOiIzNSIsInNjb3BlcyI6WyIqIl19.gPHtPsjV4VaT4q6sHyV-tIlgwJs-EDpL3Hxz5j6uTmIEjulXtXWBA2IyJ7PXKrN4FZP6Z00WkWcezjEuiTaAtXFTc8QiSdoelm6C3VbfKxiuhlZ3R13CHBT5QA3aOS1hBzZw4X6wAMLWO5wnfv3zjtCJh693AfULBpS-Xm5XWq8dIc0iXdCwpCG0LI2pgy66E6LpzpkiryEyY6Qxe2HHP5LyCAYOpJ9-iLHq_khvSjSh9rm3gunY8VA9ZlRSJz-GHyH00c6FFJrBmWBJUaq2JGaskTJgJ9QG5XUmvadZDl6cXD0XEtOCOdjw9PgfECtQIjej003GRqteA4ccJqLe6yk2fRVdIlYlWHVpkspXb-X-bsZXspS73olx5aVwXy4alvL5e9mSVE2GNUv_GP4rwOsG5NMVpryRRyWDLlWCGavl0QUYdaShFQ5Si0bF9oAM31WelHJBaorhJdrIM3HkzepW8tXUNgE0oDCcmDJ8_d4qXkdbHIs3hP2nUalOe9NWMAwv6t0icZYo7dGct78KqDqNKLjlwGV6IiTRASZS_uTeWvTlZRW0NWPCBzBYT4PnH932K5IiPuGiJF5ZXBJ1JjZx3U4gZON9iH_LfiPSzeBj1sP5-QBN2EaYAq18MzW7n-4Apq6FbAj8BQ-038BcRRJU9bBqyTlSeHpvuK_AieQ",
     * "refresh_token": "def502000dffe775ac171c85af509c1063d4c5de3485d79a375ea2ac9e6143f13a8f2a3d698b74025f4b66f85f582ed360a1e1a0833ea89e9bc13361d1cf22a061d5601817f6b6e7baf29ae4c0c048b77d7f3e87b7da8bea57a06118b4379084c4328677961b4d0264cbf3c66b569b69fd3af9d2a7acb61bf98e7fb1ce387e4506834d38f64466301761b4b881aae27f1ffa23074f8d9ce711ada0af829228881ad2cfaf6720c860763837e2fa5878b64f0920fc02d3a3ffb48af02fe871a11a3751297c31d70aa418701f00299b5be83f1766650c4000632e1610d06bce28f327646aeaa3a2b749e629a91516f64dc0e989cfbd31e84555f3a37d6e43d99cc9e48eec0006be43074536674d92a169f441dc979b464e85501227e9be6276a2d6e2eb56a1eb1cb998f1567f5340267243a31f93fd845a9b23a52dbaecc4a1333453d9e119ce9be64042c1e053edf50754a6ef3e077840774cccfa77d26b998daa43a1384d1f"
     * }
     *
     * @param Request $request
     * @return bool
     */
    public function getTokens(Request $request)
    {
        $request->request->add([
            'grant_type' => 'password',
            'client_id' => $this->client->id,
            'client_secret' => $this->client->secret,
            'username' => $request->user()->email,
            'password' => $request['password'],
            'scope' => '*',
        ]);

        $proxy = Request::create(
            '/oauth/token',
            'POST'
        );

        $response = Route::dispatch($proxy);

        if (!$response) {
            return false;
        }

        return $response;
    }

}
