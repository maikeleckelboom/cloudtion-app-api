<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\ValidationException;

/**
 * @group Auth endpoints
 */
class LoginController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * @var object
     */
    private $client;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->client = DB::table('oauth_clients')->where('id', 2)->first();
    }

    protected function sendLoginResponse(Request $request)
    {
        $this->clearLoginAttempts($request);

        if ($response = $this->authenticated($request, $this->guard()->user())) {
            return $response;
        }

        // get access_token and refresh_token
        $response = $this->getTokens($request);

        // if getting tokens failed
        if (!$response) {
            return false;
        }

        // decode Json to array
        $json_object = (array)json_decode($response->getContent());

//        $json_object['refresh_token'];

        // set new property user
        $json_object['user'] = $request->user();

        // encode array to json
        $response->setContent(json_encode($json_object));

        // return response
        return $response;

    }

    /**
     * Log the user out of the application.
     *
     * @authenticated
     * @response status=204 scenario="Success" {}
     * @response status=400 scenario="Unauthenticated" {
     *     "message": "Unauthenticated."
     * }     * @param Request $request
     *
     * @return User[]|Collection
     * @return Response
     */
    public function logout(Request $request)
    {
        return $request->wantsJson()
            ? new Response(['message' => 'Succesfully logged out.'], 204)
            : redirect('/');
    }

    /**
     * Handle a login request to the application.
     *
     * @bodyParam email email required The email of the user. Example: demo@demo.com
     * @bodyParam password password required The password of the user. Example: password
     *
     * @response status=422 scenario="Validation error" {
     *    "message": "The given data was invalid.",
     *    "errors": {
     *        "email": [
     *            "The email field is required."
     *        ]
     *    }
     * }
     *
     * @param Request $request
     * @return void
     *
     * @throws ValidationException
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }


        if ($this->attemptLogin($request)) {

            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }


    /**
     * Get the Laravel Passport Auth Tokens
     *
     * @response 200 {
     * "token_type": "Bearer",
     * "expires_in": 86398,
     * "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiMmYyZmJiMDliYjdmM2ZmMTg2MTYzMzdlMjgzNmMxYjBjZTUxZjgyOTY1YzMzNDE1MTc5M2RmMmE0YWNiZTc0NDA3YzAyMmZjZjJhODU0NzUiLCJpYXQiOjE1OTU4ODY4MTEsIm5iZiI6MTU5NTg4NjgxMSwiZXhwIjoxNTk1OTczMjA5LCJzdWIiOiIzNSIsInNjb3BlcyI6WyIqIl19.gPHtPsjV4VaT4q6sHyV-tIlgwJs-EDpL3Hxz5j6uTmIEjulXtXWBA2IyJ7PXKrN4FZP6Z00WkWcezjEuiTaAtXFTc8QiSdoelm6C3VbfKxiuhlZ3R13CHBT5QA3aOS1hBzZw4X6wAMLWO5wnfv3zjtCJh693AfULBpS-Xm5XWq8dIc0iXdCwpCG0LI2pgy66E6LpzpkiryEyY6Qxe2HHP5LyCAYOpJ9-iLHq_khvSjSh9rm3gunY8VA9ZlRSJz-GHyH00c6FFJrBmWBJUaq2JGaskTJgJ9QG5XUmvadZDl6cXD0XEtOCOdjw9PgfECtQIjej003GRqteA4ccJqLe6yk2fRVdIlYlWHVpkspXb-X-bsZXspS73olx5aVwXy4alvL5e9mSVE2GNUv_GP4rwOsG5NMVpryRRyWDLlWCGavl0QUYdaShFQ5Si0bF9oAM31WelHJBaorhJdrIM3HkzepW8tXUNgE0oDCcmDJ8_d4qXkdbHIs3hP2nUalOe9NWMAwv6t0icZYo7dGct78KqDqNKLjlwGV6IiTRASZS_uTeWvTlZRW0NWPCBzBYT4PnH932K5IiPuGiJF5ZXBJ1JjZx3U4gZON9iH_LfiPSzeBj1sP5-QBN2EaYAq18MzW7n-4Apq6FbAj8BQ-038BcRRJU9bBqyTlSeHpvuK_AieQ",
     * "refresh_token": "def502000dffe775ac171c85af509c1063d4c5de3485d79a375ea2ac9e6143f13a8f2a3d698b74025f4b66f85f582ed360a1e1a0833ea89e9bc13361d1cf22a061d5601817f6b6e7baf29ae4c0c048b77d7f3e87b7da8bea57a06118b4379084c4328677961b4d0264cbf3c66b569b69fd3af9d2a7acb61bf98e7fb1ce387e4506834d38f64466301761b4b881aae27f1ffa23074f8d9ce711ada0af829228881ad2cfaf6720c860763837e2fa5878b64f0920fc02d3a3ffb48af02fe871a11a3751297c31d70aa418701f00299b5be83f1766650c4000632e1610d06bce28f327646aeaa3a2b749e629a91516f64dc0e989cfbd31e84555f3a37d6e43d99cc9e48eec0006be43074536674d92a169f441dc979b464e85501227e9be6276a2d6e2eb56a1eb1cb998f1567f5340267243a31f93fd845a9b23a52dbaecc4a1333453d9e119ce9be64042c1e053edf50754a6ef3e077840774cccfa77d26b998daa43a1384d1f"
     * }
     *
     * @param Request $request
     * @return bool
     */
    public function getTokens(Request $request)
    {
        //  Laravel Passpport - Personal Access Token
        // 'token' => $user->createToken($request->input('device_name'))->accessToken,

        $request->request->add([
            'grant_type' => 'password',
            'client_id' => $this->client->id,
            'client_secret' => $this->client->secret,
            'username' => $request->email,
            'password' => $request->password,
            'scope' => '*',
        ]);

        $proxy = Request::create(
            '/oauth/token',
            'POST'
        );

        $response = Route::dispatch($proxy);

        if (!$response) {
            return false;
        }

        return $response;
    }
}
