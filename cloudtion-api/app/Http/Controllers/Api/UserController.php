<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class UserController extends Controller
{
    /**
     * @var object
     */
    private $client;

    /**
     *  AuthenticationController constructor
     */
    public function __construct()
    {
        $this->client = DB::table('oauth_clients')->where('id', 2)->first();
    }

    /**
     * returns all registered users
     *
     * @return User[]|Collection
     */

    public function index()
    {
        return User::all();
    }

    /**
     * Implements refresh token refreshes authentication
     *
     * @authenticated
     *
     * @response 200 {
     *      "token_type": "Bearer",
     *      "expires_in": 86398,
     *      "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiMmYyZmJiMDliYjdmM2ZmMTg2MTYzMzdlMjgzNmMxYjBjZTUxZjgyOTY1YzMzNDE1MTc5M2RmMmE0YWNiZTc0NDA3YzAyMmZjZjJhODU0NzUiLCJpYXQiOjE1OTU4ODY4MTEsIm5iZiI6MTU5NTg4NjgxMSwiZXhwIjoxNTk1OTczMjA5LCJzdWIiOiIzNSIsInNjb3BlcyI6WyIqIl19.gPHtPsjV4VaT4q6sHyV-tIlgwJs-EDpL3Hxz5j6uTmIEjulXtXWBA2IyJ7PXKrN4FZP6Z00WkWcezjEuiTaAtXFTc8QiSdoelm6C3VbfKxiuhlZ3R13CHBT5QA3aOS1hBzZw4X6wAMLWO5wnfv3zjtCJh693AfULBpS-Xm5XWq8dIc0iXdCwpCG0LI2pgy66E6LpzpkiryEyY6Qxe2HHP5LyCAYOpJ9-iLHq_khvSjSh9rm3gunY8VA9ZlRSJz-GHyH00c6FFJrBmWBJUaq2JGaskTJgJ9QG5XUmvadZDl6cXD0XEtOCOdjw9PgfECtQIjej003GRqteA4ccJqLe6yk2fRVdIlYlWHVpkspXb-X-bsZXspS73olx5aVwXy4alvL5e9mSVE2GNUv_GP4rwOsG5NMVpryRRyWDLlWCGavl0QUYdaShFQ5Si0bF9oAM31WelHJBaorhJdrIM3HkzepW8tXUNgE0oDCcmDJ8_d4qXkdbHIs3hP2nUalOe9NWMAwv6t0icZYo7dGct78KqDqNKLjlwGV6IiTRASZS_uTeWvTlZRW0NWPCBzBYT4PnH932K5IiPuGiJF5ZXBJ1JjZx3U4gZON9iH_LfiPSzeBj1sP5-QBN2EaYAq18MzW7n-4Apq6FbAj8BQ-038BcRRJU9bBqyTlSeHpvuK_AieQ",
     *      "refresh_token": "def502000dffe775ac171c85af509c1063d4c5de3485d79a375ea2ac9e6143f13a8f2a3d698b74025f4b66f85f582ed360a1e1a0833ea89e9bc13361d1cf22a061d5601817f6b6e7baf29ae4c0c048b77d7f3e87b7da8bea57a06118b4379084c4328677961b4d0264cbf3c66b569b69fd3af9d2a7acb61bf98e7fb1ce387e4506834d38f64466301761b4b881aae27f1ffa23074f8d9ce711ada0af829228881ad2cfaf6720c860763837e2fa5878b64f0920fc02d3a3ffb48af02fe871a11a3751297c31d70aa418701f00299b5be83f1766650c4000632e1610d06bce28f327646aeaa3a2b749e629a91516f64dc0e989cfbd31e84555f3a37d6e43d99cc9e48eec0006be43074536674d92a169f441dc979b464e85501227e9be6276a2d6e2eb56a1eb1cb998f1567f5340267243a31f93fd845a9b23a52dbaecc4a1333453d9e119ce9be64042c1e053edf50754a6ef3e077840774cccfa77d26b998daa43a1384d1f"
     * }
     *
     * @response status=400 scenario="Unauthenticated" {
     *     "message": "Unauthenticated."
     * }
     *
     * @param Request $request
     * @return Mixed
     */
    public function refresh(Request $request)
    {
        $request->request->add([
            'username' => '',
            'password' => '',
            'grant_type' => 'refresh_token',
            'refresh_token' => $request->refresh_token,
            'client_id' => $this->client->id,
            'client_secret' => $this->client->secret,
            'scope' => '*',
        ]);

        $proxy = Request::create(
            '/oauth/token',
            'POST'
        );

        return Route::dispatch($proxy);
    }

    /**
     * returns all user actions
     *
     * @return mixed
     */
    public function activities()
    {
        return auth()->user()->actions;
    }
}
