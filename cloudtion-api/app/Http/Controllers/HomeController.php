<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use App\Server;
use App\User;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        $users = User::all();


        return view('home', compact('users'));
    }
}
