<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rules\Unique;


/**
 * @method valid()
 */
class CreateServerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => [
                "required",
                \Illuminate\Validation\Rule::unique('servers')->where('user_id', auth()->user()->id),
                "in:linux,centos,ubuntu,mariadb"
            ],
            'username' => 'required|max:255',
            'password' => 'required|max:255',
        ];
    }

    /**
     * @return array|void
     */
    public function messages()
    {
        return [
            'type.required' => 'Server type is verplicht',
            'username.required' => 'Gebruikersnaam is verplicht',
            'password.required' => 'Wachtwoord is verplicht',
        ];
    }
}
