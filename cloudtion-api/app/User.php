<?php

namespace App;

use App\Notifications\PasswordResetNotification;
use App\Notifications\VerifyEmail;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Spatie\Activitylog\Traits\CausesActivity;


/**
 * @method static where(string $string, $email)
 * @method static create(array $array)
 * @method static find($input)
 */
class User extends Authenticatable implements MustVerifyEmail
{

    use Notifiable, HasApiTokens, CausesActivity;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail());
    }

    /**
     * Send the password reset notification.
     *
     * @param string $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordResetNotification($token));
    }

    /**
     * The servers that belong to the user.
     *
     * @return HasMany
     */
    public function servers()
    {
        return $this->hasMany('App\Server');
    }

    /**
     * The notifications that belong to the user.
     *
     * @return HasMany
     */
    public function notifications()
    {
        return $this->hasMany('App\Notification');
    }
}
