<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\CausesActivity;

/**
 * @method static create(Http\Requests\CreateServerRequest $request)
 * @method static where(string $string, $id)
 * @method static firstOrCreate(array $input)
 * @method static findOrFail($id)
 * @method static find($id)
 * @method static whereKey($id)
 */
class Server extends Model
{
    use CausesActivity;

    protected $table = 'servers';

    protected $fillable = [
        'user_id', 'server_id', 'order', 'initialized', 'type', 'status', 'username', 'password'
    ];

    protected $hidden = ['created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }


}
