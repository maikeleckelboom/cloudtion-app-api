<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api', 'as' => 'api.'], function () {
    Route::get('users', 'UserController@index')->name('users');
    Route::post('refresh', 'UserController@refresh')->name('refresh');
});


Route::group(['namespace' => 'Api', 'as' => 'api.'], function () {

    Route::group(['middleware' => ['auth:api']], function () {

        Route::get('activities', 'UserController@activities')->name('user.activities');

    });

});

/**
 * Api\Auth Routes
 *
 */
Route::group(['namespace' => 'Api\Auth', 'as' => 'api.'], function () {

    Route::post('login', 'LoginController@login')->name('login');

    Route::post('register', 'RegisterController@register')->name('register');

    Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.forgot');

    Route::post('password/reset', 'ResetPasswordController@reset')->name('password.reset');

    Route::group(['middleware' => ['auth:api']], function () {

        Route::get('email/verify/{hash}', 'VerificationController@verify')->name('verification.verify');

        Route::get('email/resend', 'VerificationController@resend')->name('verification.resend');

        Route::get('user', 'AuthenticationController@user')->name('user');

        Route::post('logout', 'LoginController@logout')->name('logout');

    });

});


/**
 * Api\Server Routes
 *
 */
Route::group(['namespace' => 'Api\Server', 'as' => 'api.'], function () {

    Route::group(['middleware' => ['auth:api', 'verified']], function () {

        Route::post('messages', 'ServerMessagesController@store')->name('message.store');

        Route::get('messages', 'ServerMessagesController@index')->name('message.index');

        Route::get('servers', 'ServerResourceController@index')->name('servers.index');

        Route::post('servers', 'ServerResourceController@store')->name('server.store');

        Route::post('servers/{server}/index', 'ServerResourceController@update_index')->name('server.store');

        Route::post('server/reset/{server}', 'ServerResourceController@reset')->name('reset');

        Route::patch('servers/{server}', 'ServerResourceController@update')->name('server.update');

        Route::delete('servers/{server}', 'ServerResourceController@destroy')->name('server.destroy');

        // Server System Controllers
        Route::post('system/servers/{server}', 'ServerSystemController@update');

        Route::patch('system/servers/{server}', 'ServerSystemController@update');

    });

});


