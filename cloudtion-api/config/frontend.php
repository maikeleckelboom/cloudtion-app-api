<?php

return [
    'email_verify_url' => env('FRONTPAGE_VERIFY_URL', 'http://localhost:8080/verify/'),
    'reset_password_url' => env('FRONTPAGE_RESET_PASSWORD_URL', 'http://localhost:8080/reset-password/'),
];
