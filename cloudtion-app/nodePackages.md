### yarn installs
###### yarn add vue-password-strength-meter zxcvbn

### npm installs
###### npm install vee-validate --save
###### npm install vue-page-transition
###### npm install velocityjs
###### npm install vue2-simplert@latest --save-dev
###### npm i -S vodal
###### npm install overlayscrollbars
###### npm install overlayscrollbars-vue
###### npm i --save os-vue
###### npm install --save vue-animejs
###### npm install --save vue2-animate
###### npm install vue-focus --save
###### npm i vue2-transitions


#### uninstalled
###### npm uninstall validator
###### npm install pretty-checkbox
