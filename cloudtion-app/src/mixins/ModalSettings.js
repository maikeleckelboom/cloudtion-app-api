export const ModalSettings = {
    data() {
        return {
            modal: {
                name: 'Stop of Start server',
                desc: ``,
                transition: "nice-modal-fade",
                width: "100%",
                height: 'auto',
                maxWidth: 1028,
                minHeight: 740,
                minWidth: 320,
                delay: 100,
                adaptive: true,
                clickToClose: true,
                scrollable: false,
                showBackgroundMask: false,
                go_back: require('./../assets/files/svg/arrow.svg')
            },
        }
    }
}

