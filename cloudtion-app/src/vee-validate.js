import Vue from "vue"
import axios from "axios"
import store from "./store"
import * as rules from 'vee-validate/dist/rules'
import {
    setInteractionMode,
    configure,
    ValidationObserver,
    ValidationProvider,
    extend
} from 'vee-validate'

// Install components globally
Vue.component('ValidationObserver', ValidationObserver)
Vue.component('ValidationProvider', ValidationProvider)

// Loop to initialize all rules
Object.keys(rules).forEach(rule => {
    extend(rule, rules[rule]);
});

// Interaction mode
setInteractionMode('eager');

// Configure CSS Classes
configure('classes', {
    validate() {
        return {
            classes: {/* all */}
        };
    },
    message: 'Class invalid'
});

/**
 - unique email server-side validation
 - checks if the e-mailadress already exists
 **/

// Empty array to store users
let all_users = []

// Promise that returns all the users
all_users.get_emails = function () {

    let promise = new Promise(function (resolve) {
        let users = axios.get(process.env.VUE_APP_API_URL + "users")
        resolve(users)
    })

    return promise
};

// Resolve Promise of all_users and filter out e-mailadresses
all_users.get_emails().then((response) => {

    let users = response.data

    let user_emails = users.map(function (user) {
        return user.email
    });

    return user_emails
})

async function toggle_spinner_off() {
    store.commit('application/setLoading', false)
}

// Async function applies logic to check if given e-mailadress is unique
async function check_email(email) {

    // Start spinner loading animation
    store.commit('application/setLoading', true)

    // Fetch JSON data
    let response = await fetch(process.env.VUE_APP_API_URL + "users");

    // Await respsone
    let users = await response.json()

    // Filter users array return all email-adresses
    let emails = users.map(function (user) {
        return user.email
    });

    // Empty array for email errors
    let email_errors = []

    // Checks if email-adres is already already taken.
    if (emails.indexOf(email) !== -1) {
        email_errors.push("this email-adres is already already taken")
    }

    // Return true if email_errors array has no items
    const isValid = !email_errors.length;

    // If email already exists return object with valid: false
    if (!isValid) {
        return {
            valid: false,
            message: email_errors,
        };

    }

    // If E-mailadress is unique | unkown return valid: true
    return {
        valid: true
    };

}



// Unique E-mail server-side validation
extend("unique", {
    validate: async value => {

        const response = await check_email(value);

        await toggle_spinner_off();

        if (response.valid) {
            return true;
        }

        return {
            valid: false,
        };

    },

    message: "Dit e-mailadres is staat al geregistreerd",



});

// Not_unique E-mail server-side validation
extend("not_unique", {
    validate: async value => {

        const response = await check_email(value);

        await toggle_spinner_off();

        if (response.valid) {
            return {
                valid: false,
            };
        }

        return true

    },

    message: "Dit e-mailadres is niet bij ons bekend",
});

// Extend email rule
extend("email", {
    message: 'Dit is geen geldig e-mailadres'
});

// Extend required rule
extend('required', {
    validate(value) {
        return {
            required: true,
            valid: ['', null, undefined].indexOf(value) === -1
        };
    },

    // This rule reports the `required` state of the field.
    computesRequired: true,
    message: 'Dit veld is verplicht'
});


extend("is_not_one_of", {
    computesRequired: true,

    validate: (value) => {
        if (value === 'dba' ||
            value === 'root' ||
            value === 'student') {
            return false;
        }
        return true;
    },

    message: "Het {_field_} veld mag niet de volgende woorden bevatten: dba, root, student",
})

extend("required_type", {
    computesRequired: true,

    validate: (value) => {
        if (value === 'linux' ||
            value === 'centos' ||
            value === 'ubuntu' ||
            value === 'mariadb') {
            return true;
        }
        return false;
    },

    message: "Selecteer één van de bovenstaande opties om verder te gaan",
})

// Override the default message.
extend('alpha', {
    computesRequired: true,
    message: 'Het veld mag alleen alfabetische tekens bevatten'
});

// Extend minmax rule
extend('minmax', {
    validate(value, {min, max}) {
        return value.length >= min && value.length <= max;
    },
    params: ['min', 'max'],
    message: 'Het {_field_} veld moet bestaan uit minimaal {min} en maximaal {max} tekens'
});

// Extend Custom minmax rule
extend('custom_minmax', {
    validate(value) {
        return value.length >= 2 && value.length <= 8;
    },
    params: ['min', 'max'],
    message: 'Het {_field_} veld moet bestaan uit minimaal 2 en maximaal 8 tekens'
});

// Extend password rule
extend('password', {
    params: ['target'],
    validate(value, {target}) {
        return value === target;
    },
    message: 'De wachtwoorden komen niet overeen'
});
