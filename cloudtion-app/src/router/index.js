import Vue from 'vue'
import VueRouter from "vue-router";
import VuePageTransition from 'vue-page-transition'

Vue.use(VuePageTransition);
Vue.use(VueRouter)

const guest = (to, from, next) => {
    if (!localStorage.getItem("access_token")) {
        return next();
    } else {
        return next("/dashboard");
    }
};
const auth = (to, from, next) => {
    if (localStorage.getItem("access_token")) {
        return next();
    } else {
        return next("/login");
    }
};

const routes = [

    {
        path: '/',
        redirect: {
            name: 'Login'
        }
    },
    {
        path: "/login",
        name: "Login",
        meta: {
            title: 'Login',
        },
        beforeEnter: guest,
        component: () =>
            import(/* webpackChunkName: "login" */ "../views/auth/Login.vue")
    },
    {
        path: "/register",
        name: "Register",
        meta: {
            title: 'Register',
        },
        beforeEnter: guest,
        component: () =>
            import(/* webpackChunkName: "register" */ "../views/auth/Register.vue")
    },
    {
        path: "/forgot-password",
        name: "ForgotPassword",
        meta: {
            title: 'Forgot Password',
        },
        beforeEnter: guest,
        component: () =>
            import(/* webpackChunkName: "forgot-password" */ "../views/auth/ForgotPassword.vue")
    },
    {
        path: "/reset-password",
        name: "ResetPassword",
        meta: {
            title: 'Reset Password',
        },
        beforeEnter: guest,
        component: () =>
            import(/* webpackChunkName: "reset-password" */ "../views/auth/ResetPassword.vue")
    },
    {
        path: "/verify/:hash",
        name: "Verify",
        props: true,
        meta: {
            title: 'Verify',
        },
        beforeEnter: auth,
        component: () =>
            import(/* webpackChunkName: "verify" */ "../views/auth/Verify.vue")
    },
    {
        path: "/dashboard",
        name: "Dashboard",
        meta: {
            title: 'Dashboard',
        },
        beforeEnter: auth,
        component: () =>
            import(/* webpackChunkName: "dashboard" */ "../views/Dashboard.vue")
    },
    {
        path: "/example",
        name: "Example",
        // beforeEnter: guest,
        component: () =>
            import(/* webpackChunkName: "dashboard" */ "../views/Example.vue")
    },
];


const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes
});


// This callback runs before every route change, including on page load.
router.beforeEach((to, from, next) => {
    // This goes through the matched routes from last to first, finding the closest route with a title.
    // eg. if we have /some/deep/nested/route and /some, /deep, and /nested have titles, nested's will be chosen.
    const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);

    // Find the nearest route element with meta tags.
    const nearestWithMeta = to.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);
    // const previousNearestWithMeta = from.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);

    // If a route with a title was found, set the document (page) title to that value.
    if(nearestWithTitle) document.title = nearestWithTitle.meta.title;

    // Remove any stale meta tags from the document using the key attribute we set below.
    Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(el => el.parentNode.removeChild(el));

    // Skip rendering meta tags if there are none.
    if(!nearestWithMeta) return next();

    // Turn the meta tag definitions into actual elements in the head.
    nearestWithMeta.meta.metaTags.map(tagDef => {
        const tag = document.createElement('meta');

        Object.keys(tagDef).forEach(key => {
            tag.setAttribute(key, tagDef[key]);
        });

        // We use this to track which meta tags we create, so we don't interfere with other ones.
        tag.setAttribute('data-vue-router-controlled', '');

        return tag;
    })
        // Add the meta tags to the document head.
        .forEach(tag => document.head.appendChild(tag));

    next();
});

export default router
