export default  {
    setMessages(state, payload) {
        let modified_payload = payload.reverse().sort().slice(0, 1)
        state.messages = modified_payload;
    },
}
