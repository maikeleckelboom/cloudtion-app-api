import axios from "axios";

export default {
    getMessages({commit}) {
        return new Promise((resolve, reject) => {
            axios
                .get(process.env.VUE_APP_API_URL + "messages")
                .then(response => {
                    commit("setMessages", response.data)

                    resolve(response)
                })
                .catch((error) => {
                    reject(error)
                });
        });
    },
    setMessages({commit}, data) {
        return new Promise((resolve, reject) => {
            axios
                .post(process.env.VUE_APP_API_URL + "messages" + data)
                .then(response => {
                    commit("setMessages", response.data)
                    resolve(response)
                })
                .catch((error) => {
                    reject(error)
                });
        });
    },
}
