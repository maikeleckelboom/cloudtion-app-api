import Vue from "vue";
import Vuex from "vuex";

// Modules
import authentication from "./authentication/index"
import application from "./application/index"
import servers from "./servers/index"
import activities from "./activities/index"

Vue.use(Vuex);

export default new Vuex.Store({


    state: {
        errors: []
    },

    getters: {
        errors: state => state.errors
    },

    mutations: {
        setErrors(state, errors) {
            state.errors = errors;
        }
    },

    actions: {},

    modules: {
        application,
        authentication,
        servers,
        activities
    },

});
