import axios from "axios";

export default {
    getUserData({commit}) {
        return new Promise((resolve, reject) => {
            axios
                .get(process.env.VUE_APP_API_URL + "user")
                .then(response => {
                    commit("setUserData", response.data)
                    resolve(response)
                })
                .catch((error) => {
                    localStorage.removeItem("access_token")
                    localStorage.removeItem("refresh_token")
                    reject(error)
                });
        });
    },
    sendLoginRequest({commit}, data) {
        return new Promise((resolve, reject) => {
            commit("setErrors", {}, {root: true});
            axios
                .post(process.env.VUE_APP_API_URL + "login", data)
                .then(response => {
                    commit("setUserData", response.data.user)
                    localStorage.setItem("access_token", response.data.access_token)
                    localStorage.setItem("refresh_token", response.data.refresh_token)
                    resolve(response)
                })
                .catch((error) => {
                    reject(error)
                })
        });
    },
    sendRegisterRequest({commit}, data) {
        return new Promise((resolve, reject) => {
            commit("setErrors", {}, {root: true});
            axios
                .post(process.env.VUE_APP_API_URL + "register", data)
                .then(response => {
                    commit("setUserData", response.data.user)
                    localStorage.setItem("access_token", response.data.access_token)
                    localStorage.setItem("refresh_token", response.data.refresh_token)
                    resolve(response)
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },
    sendLogoutRequest({commit}) {
        return new Promise((resolve, reject) => {
            axios
                .post(process.env.VUE_APP_API_URL + "logout")
                .then((response) => {
                    commit("setUserData", null)
                    localStorage.removeItem("access_token")
                    localStorage.removeItem("refresh_token")
                    resolve(response)
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },
    sendVerifyResendRequest() {
        return new Promise((resolve, reject) => {
            axios
                .get(process.env.VUE_APP_API_URL + "email/resend")
                .then((response) => {
                    resolve(response)
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },
    sendVerifyRequest({commit}, hash) {
        return new Promise((resolve, reject) => {
            axios
                .get(process.env.VUE_APP_API_URL + "email/verify/" + hash)
                .then((response) => {
                    commit("setUserData")
                    resolve(response)
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },
    sendForgotPasswordRequest({commit}, data) {
        return new Promise((resolve, reject) => {
            commit("setErrors", {}, {root: true});
            axios
                .post(process.env.VUE_APP_API_URL + "password/email", data)
                .then((response) => {
                    resolve(response)
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },
    sendResetPasswordRequest({commit}, data) {
        return new Promise((resolve, reject) => {
            commit("setErrors", {}, {root: true});
            axios
                .post(process.env.VUE_APP_API_URL + "password/reset", data)
                .then(response => {
                    commit("setUserData", response.data.user)
                    resolve(response)
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },
    sendRefreshTokenRequest({commit}) {
        return new Promise((resolve, reject) => {
            commit("setErrors", {}, {root: true});
            const refresh_token = localStorage.getItem('refresh_token');
            const data = {refresh_token: refresh_token};
            axios
                .post(process.env.VUE_APP_API_URL + "refresh", data)
                .then((response) => {
                    localStorage.setItem('access_token', response.data.access_token)
                    localStorage.setItem('refresh_token', response.data.refresh_token)
                    resolve(response)
                })
                .catch((error) => {
                    reject(error)
                })
        })
    }
}
