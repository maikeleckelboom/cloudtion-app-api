import axios from "axios";

export default {
    getActivities({commit}) {
        return new Promise((resolve, reject) => {
            axios
                .get(process.env.VUE_APP_API_URL + "activities")
                .then(response => {
                    commit("setActivities", response.data)
                    resolve(response)
                })
                .catch((error) => {
                    reject(error)
                });
        });
    },
}
