export default  {
    setActivities(state, payload) {
        let modified_payload = payload.reverse().sort().slice(0, 3)
        state.activities = modified_payload;
    },
}
