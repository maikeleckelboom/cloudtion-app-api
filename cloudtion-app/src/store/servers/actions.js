import axios from "axios";

export default {
    updateItemOrder({commit}, data) {
        commit("setErrors", {}, {root: true});
        return new Promise((resolve, reject) => {
            axios
                .patch(process.env.VUE_APP_API_URL + "servers/", data)
                .then((response) => {
                    resolve(response)
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },
    sendUpdateStatusRequest({commit}, data) {
        commit("setErrors", {}, {root: true});

        const id = data.id;
        const status = {status: data.status};

        return new Promise((resolve, reject) => {
            axios
                .patch(process.env.VUE_APP_API_URL + "servers/" + id, status)
                .then((response) => {
                    resolve(response)
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },
    sendResetServerRequest({commit}, data) {
        commit("setErrors", {}, {root: true});

        return new Promise((resolve, reject) => {
            axios
                .post(process.env.VUE_APP_API_URL + "server/reset/" + data)
                .then((response) => {
                    resolve(response)
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },
    setDesiredOption({commit}, data) {
        let promise = new Promise((resolve) => {
            commit('setDesiredOption', data),
                resolve(data)
        })

        return promise;
    },
    setDesiredModal({commit}, data) {
        let promise = new Promise((resolve) => {
            commit('setDesiredModal', data)
            resolve(data)
        })

        return promise.then((response) => {
            commit('getDisiredServerData', response)
        })
    },

    getServerRequest({commit}) {
        return new Promise((resolve, reject) => {
            axios
                .get(process.env.VUE_APP_API_URL + "servers")
                .then(response => {
                    commit("getServers", response.data)
                    resolve(response)
                })
                .catch((error) => {
                    reject(error)
                });
        });
    },

    sendServerRequest({commit}, data) {
        return new Promise((resolve, reject) => {
            commit("setErrors", {}, {root: true});
            axios
                .post(process.env.VUE_APP_API_URL + "servers", data)
                .then((response) => {
                    commit("setNewServer", response.data);
                    resolve(response)
                })
                .catch((error) => {
                    reject(error)
                })
        });
    },

    deleteServerRequest({commit}, data) {
        return new Promise((resolve, reject) => {
            axios
                .delete(process.env.VUE_APP_API_URL + "servers/" + data)
                .then(response => {
                    commit("removeServer", data)
                    resolve(response)
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },
}
