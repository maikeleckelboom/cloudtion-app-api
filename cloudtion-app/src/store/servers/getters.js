export default {

    server(state) {
        return state.server
    },

    servers(state) {
        return state.servers;
    },

    server_types(state) {
        return state.server_types = state.servers.map(server => server.type)
    },

    waitlist(state) {
        return state.waitlist;
    },

    desired_modal(state) {
        return state.desired_modal;
    },

    desired_option(state) {
        return state.desired_option;
    },
}
