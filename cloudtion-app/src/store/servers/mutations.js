export default {
    getWaitlist(state) {
        let pending = state.servers.filter(server => server.status = 'pending');
        state.waitlist = pending
    },
    getServerTypes(state) {
        state.server_types = state.servers.map(server => server.type)
    },

    getDisiredServerData(state, payload) {
        let server_type = payload
        let server_data = state.servers.filter(server => server.type === server_type)
        state.server = server_data[0]
    },

    setDesiredOption(state, payload) {
        state.desired_option = payload;
    },

    setDesiredModal(state, payload) {
        state.desired_modal = payload;
    },

    setNewServer(state, payload) {
        const server = payload;
        server.icon = require('@/assets/files/servers/' + server.type + '.png');
        server.reset = "reset";
        server.delete = 'verwijderen';
        state.servers.push(server);
    },

    getServers(state, payload) {
        const servers = payload;
        servers.forEach((server) => {
            server.icon = require('@/assets/files/servers/' + server.type + '.png');
            server.reset = "reset";
            server.delete = 'verwijderen';
            state.servers.push(server);
        });


    },

    removeServer(state, payload) {
        // Find corresponding index of the server in servers array
        const index = state.servers.findIndex(server => server.id === payload)
        state.servers.splice(index, 1)
    },
}
