export default {
    setLoading: function (store, payload) {
        return new Promise((resolve) => {
            store.commit('application/setLoading', payload);
            resolve(true)
        })
    },
}
