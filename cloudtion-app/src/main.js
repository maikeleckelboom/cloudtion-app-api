import Vue from 'vue'
import App from './App.vue'
import store from './store'
import axios from "axios"
import VModal from 'vue-js-modal'
import velocityPlugin from 'velocity-vue'
import router from './router'
import VTooltip from 'v-tooltip'
import VueProgressBar from 'vue-progressbar'
import Toast from "vue-toastification"
import VueLocalStorage from 'vue-localstorage'
import VueTimeago from 'vue-timeago'
import Clipboard from 'v-clipboard'
import Chartkick from 'vue-chartkick'
import Chart from 'chart.js'

import "vue-toastification/dist/index.css"
import 'vue-select/dist/vue-select.css'
import "./vee-validate"
import "./assets/foundation/css/foundation.min.css"
import "./assets/css/app.css"
import "./assets/js/app"


/**
 *  Global vendor options
 */
const toast_options = {
    position: "top-center",
    timeout: 8200,
    closeOnClick: false,
    pauseOnFocusLoss: true,
    pauseOnHover: true,
    draggable: true,
    draggablePercent: 0.3,
    closeButton: 'a',
    showCloseButtonOnHover: false,
    rtl: false,
    hideProgressBar: false,
    icon: true,
    maxToasts: 8,
    toastClassName: "toast-override",
    bodyClassName: ["toast-body-override"]
};

const progress_bar_options = {
    location: 'top',
    color: '#3882ff',
    failedColor: '#874b4b',
    thickness: '0.2em',
    autoRevert: true,
    inverse: false,
}

const time_ago_options = {
    name: 'Timeago', // Component name, `Timeago` by default
    locale: 'en',
    locales: {
        'zh-CN': require('date-fns/locale/zh_cn'),
        ja: require('date-fns/locale/ja')
    }
}

/**
 *  Vue usages
 */
Vue.config.productionTip = false
Vue.use(VModal)
Vue.use(VTooltip)
Vue.use(velocityPlugin)
Vue.use(VueProgressBar, progress_bar_options)
Vue.use(Toast, toast_options)
Vue.use(VueLocalStorage)
Vue.use(VueTimeago, time_ago_options)
Vue.use(Clipboard)
Vue.use(Chartkick.use(Chart))
Vue.use(require('vue-moment'));


/**
 * Axios interceptor to refresh auth token
 */
import createAuthRefreshInterceptor from 'axios-auth-refresh';

import './registerServiceWorker'

// Obtain the fresh token each time the function is called
function getRefreshToken() {
    let refresh_token = localStorage.getItem('refresh_token');
    if (refresh_token === null) {
        console.log("The refresh token cannot be null")
        localStorage.removeItem("access_Token")
        localStorage.removeItem("refresh_Token")
        // TODO: re-direct via Vuex Store
    }
    return {refresh_token: refresh_token}
}

// Function that will be called to refresh authorization
const refreshAuthLogic = failedRequest =>
    axios.post(process.env.VUE_APP_API_URL + 'refresh', getRefreshToken())
        .then(tokenRefreshResponse => {
            localStorage.setItem('access_token', tokenRefreshResponse.data.access_token);
            localStorage.setItem('refresh_token', tokenRefreshResponse.data.refresh_token);
            failedRequest.response.config.headers['Authorization'] = 'Bearer ' + tokenRefreshResponse.data.access_token;
            return Promise.resolve();
        });

// Instantiate the interceptor (you can chain it as it returns the axios instance)
createAuthRefreshInterceptor(axios, refreshAuthLogic);

/**
 *  attach a token to every request mad at every axios call
 */
axios.interceptors.request.use(function (config) {
    config.headers.common = {
        Authorization: `Bearer ${localStorage.getItem("access_token")}`,
        "Content-Type": "application/json",
        Accept: "application/json"
    };

    return config;
});

/**
 *  Global Vue instance
 */
new Vue({
    router,
    store,
    render: h => h(App)
}).$mount("#app");
